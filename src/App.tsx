import React, {useState } from 'react';
import "./app.css";
import Input from './Input';
import Button from './button';
import { RiDeleteBin6Line,RiEdit2Fill } from "react-icons/ri";
import useLocalStorage from "./getlocal";
import firebase from "./firebase_config";
import {  NavLink } from 'react-router-dom';

const App=()=>{
    
    const [newInputData,setNewInputData]=useState<string>("");
    const [edititem,updateedititem]=useState<boolean>(true);
    const [editinputid,seteditinputid]=useState<string>("");
    
    const [list, isloading]=useLocalStorage(); //custom hooks
    
    function getValue(event: { target: { value: React.SetStateAction<string>; }; }){   
        setNewInputData(event.target.value);
    };

    const updateData=()=>{
        if(!newInputData){
        alert('Please Fill The Data');
        }
        else if(newInputData && !edititem){
            const todoref=firebase.database().ref("TODO");
            todoref.child(editinputid).update({
                name:newInputData,
            })
        updateedititem(true);
        setNewInputData('');
        seteditinputid("");
        }
        else
        {
        const todoref =firebase.database().ref("TODO");
        const todo ={name:newInputData,done:false}
        todoref.push(todo);
        setNewInputData("");  
        }
        };

    const deleteEle=(index: { id: string; })=>{
       const todoref = firebase.database().ref("TODO").child(index.id);
       todoref.remove();    
    }

    const editEle=(id:string)=>{
        let edit=list.find((ele: { id: string; })=>{
             return ele.id===id;
        });
        //console.log("edit ",edit);
        updateedititem(false);
        setNewInputData(edit.name);
        seteditinputid(id);
    }
    const cutit=(linelist:any)=>{
        const todoref= firebase.database().ref('TODO').child(linelist.id);
        todoref.update({
            done:!linelist.done,
        })
    
    }
    return (
    <div className="Divbody">
        <div>
        <button className="Logoutbutton"><NavLink to="/" style={{ textDecoration: 'none' }} className="Logoutbuttonlink">Logout</NavLink></button>
        </div>
        <Input data={newInputData} value={getValue} loading={isloading}/>
        <div>
            { list.map((listitems: { done?: boolean; name?: string; id: string; },index: React.Key | null | undefined)=>{
                return (<>
                
                <div className="Showmain" key={index}>
                    <div className="Show1">
                        <input type="checkbox" onClick={()=>cutit(listitems)}></input>
                    </div>
                    <div className="Show2">
                        <h3 style={{textDecoration: listitems.done ? "line-through" : "none"}}>{listitems.name}</h3>
                    </div>
                    <div className="Show3">
                        <span title="Delete Item" onClick={()=>deleteEle(listitems)}><RiDeleteBin6Line/></span>
                    </div>
                    <div className="Show4">
                        <span title="Edit Item" onClick={()=>editEle(listitems.id)}><RiEdit2Fill/></span>
                    </div>
                </div>
                </>)
            
            })}
        </div>
        {
            edititem ? <Button data={updateData}/>:<button className="btn" onClick={updateData}>Edit</button> 
        }
    </div>
    )
    }

export default App;