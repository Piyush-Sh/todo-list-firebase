import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
var button = false;
const User=()=>{
      
    const [email,updateemail]=useState<string>("");
    const [password,updatepassword]=useState<string>("");
    const [error,seterror] =useState<String|null>(null);
    const [friendList,setfriendList]=useState<any[]>([]);
    
    useEffect(()=>{
        button=false;
        const userFun= async ()=>{
        const responce=await fetch('https://todo-list-64f41-default-rtdb.firebaseio.com/User.json');
        const responseData = await responce.json();
        const userList=[];
        for(const key in responseData){
            userList.push({
                id:key,
                email:String(responseData[key].email),
                password:String(responseData[key].password),
            })
        }
        setfriendList(userList);
        //console.log(userList);
    };
    userFun();
    },[])
    
 
    
    function getValue1(event:any){   
        updateemail(event.target.value);
        seterror(null);

    }
      function getValue2(event:any){   
        updatepassword(event.target.value);
        seterror(null);
    }
    
    function handle(e: any){
        e.preventDefault();
        friendList.map((friend)=>
            {
            if(friend.email === email && friend.password === password) 
            {
                button=true;
            }
            return button;
            }
            )
            //console.log("button",button);
            button ? seterror("User Id and Password Verified") : seterror("Wrong User Id and Password");
        };
    return(
        
        <div>
            <div className="Logindiv">
            <form onSubmit={handle}>
            <div><h2>Login Form</h2></div>
        <div>
            <label htmlFor="email" id="email" className="Loginlabel">Email</label>
            <input className="Logininput1" type="e-mail" name="email" value={email} onChange={getValue1} required/>
        </div>
            <br/>
        <div><label htmlFor="password" id="password" className="Loginlabel">Password</label>
            <input className="Logininput2" type="password" name="password" value={password} onChange={getValue2} required/>
        </div>    
            <br/>
        <div>    
            {console.log(button)}
           {button ? <button type="submit" className="Loginbutton"><Link to="/status" style={{ textDecoration: 'none' }} className="Loginbuttonlink">Login</Link></button> :<button className="Loginbutton" type="submit">Login</button> }
        </div>
        {button
            ?
           
            <div className="Errorkdiv1"><h2 className="Errorheading1">{error}</h2></div>
            :
            
           <div className="Errorkdiv"><h2 className="Errorheading">{error}</h2></div>
        }  
        <div>
            <button className="Loginbutton"><Link to="/Register" style={{ textDecoration: 'none' }} className="Loginbuttonlink">Register</Link></button>
        </div>
           </form>
        </div>
        </div>

    )


}
export default User;