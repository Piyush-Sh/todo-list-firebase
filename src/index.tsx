import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter,Route, Switch } from 'react-router-dom';
import App from "./App";
import User from "./Login";
import Register from './Register';

const Index=()=>{
    return(
    <div>
    <BrowserRouter>
    <Switch>
        <Route exact path="/" component={User} />
        <Route exact path="/status" component={App} />
        <Route exact path="/register" component={Register} />
    </Switch>
    </BrowserRouter>
    </div>
    )
}
ReactDOM.render(<Index/>,document.getElementById("root"));