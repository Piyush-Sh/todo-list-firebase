import  { useEffect, useState } from "react";
import firebase from "./firebase_config";

const useLocalStorage=()=>{
    const [item,setitem]=useState<object[]>([]);
    const [isloading,setisloading]=useState<boolean>(true);
    useEffect(()=>{
        
        setisloading(true);
        const todoref = firebase.database().ref("TODO");
        todoref.on('value',(snapshot)=>{
            const todo= snapshot.val();
            const Items =[];
            for(let id in todo ){
                Items.push({id,...todo[id]})
            }
           setitem(Items);
           setisloading(false);
        })
},[])
    
    //console.log("getlocal firsbase",item);
    return [JSON.parse(JSON.stringify(item)),isloading];
}
export default useLocalStorage;