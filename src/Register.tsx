
import React, { useState } from 'react';
import { NavLink, useHistory, withRouter } from 'react-router-dom';
import firebase from "./firebase_config";

const Register=()=>{
    const [email,setEmail]=useState<string>("");
    const [password,setPassword]=useState<string | number >("");
    const [cpassword,cSetPassword]=useState<string | number >("");
    const [complete,setcomplete]=useState<boolean>(false);
    const [error , seterror]=useState<string|null>("");
    const history=useHistory();
     
    function getValue1(event: { target: { value: React.SetStateAction<string>; }; }){   
        setEmail(event.target.value);
        seterror(null);

    }
      function getValue2(event: { target: { value: React.SetStateAction<string | number>; }; }){   
        setPassword(event.target.value);
        seterror(null);
    }
    function getValue3(event: { target: { value: React.SetStateAction<string | number>; }; }){   
        cSetPassword(event.target.value);
        seterror(null);
    }

    const handle=(event: any)=>{
        event.preventDefault();
            if(password!==cpassword){
                seterror("Password dose not match");
            }
            else{
                const todoref = firebase.database().ref("User");
                const todo ={email:email,password:password}
                todoref.push(todo);
                setcomplete(true);
                seterror("User created! Click on Login")
                setEmail("");
                setPassword("");
                cSetPassword("");
            }
    }
    
    return(
    <div className="Registerdiv">
        <form onSubmit={handle}>
            <div><h2>Signup</h2></div>
            <div>
                <label htmlFor="email" id="email" className="Registerlabel">Email</label>
                <input className="Registerinput1" type="email" name="email" value={email} onChange={getValue1} required/>
            </div>
            <br/>
            <div>
                <label htmlFor="password" id="password" className="Registerlabel">Password</label>
                <input className="Registerinput2" type="password" name="password" value={password} onChange={getValue2} required/>
            </div>   
            
            <div>
                <label htmlFor="cpassword" id="cpassword" className="Registerlabel">Confirm Password</label>
                <input className="Registerinput3" type="password" name="cpassword" value={cpassword} onChange={getValue3} required/>
            </div> {console.log(complete)}
            <div>
           {complete ? ()=>{history.push("/")} :<button className="Registerbutton" type="submit">Signup</button> }
           </div>
           <div>
               
        {complete
            ?
           <div className="Errorkdiv1"><h2 className="Errorheading1">{error}</h2></div>
           :
           <div className="Errorkdiv"><h2 className="Errorheading">{error}</h2></div>
        }  
           </div>
           <div><button type="button" className="Registerbutton"><NavLink to="/" style={{ textDecoration: 'none' }}>Login</NavLink></button>
           </div>
        </form>
    </div>
    )


}

export default withRouter(Register);

